class Helmet {
  String title;
  String subtitle;
  String price;
  String imageUrl;
  List<String> detailUrl;
  Helmet(this.title, this.subtitle, this.price, this.imageUrl, this.detailUrl);
  static List<Helmet> generateHelmet() {
    return [
      Helmet('Pista GP RR - Soleluna', 'Racing', '\$1,359,96',
          'assets/images/arrival1.png', [
        'assets/images/arrival1.png',
        'assets/images/detail1.png',
        'assets/images/detail3.png'
      ]),
      Helmet('Pista GP RR - Carbon', 'Racing', '\$1,449,96',
          'assets/images/arrival2.png', [
        'assets/images/arrival2.png',
        'assets/images/detail2.png',
        'assets/images/detail4.png'
      ]),
      Helmet('Pista GP RR - Soleluna', 'Racing', '\$1,359,96',
          'assets/images/arrival1.png', [
        'assets/images/arrival1.png',
        'assets/images/detail1.png',
        'assets/images/detail3.png'
      ]),
      Helmet('Pista GP RR - Carbon', 'Racing', '\$1,449,96',
          'assets/images/arrival2.png', [
        'assets/images/arrival2.png',
        'assets/images/detail2.png',
        'assets/images/detail4.png'
      ]),
    ];
  }
}
