import 'package:flutter/material.dart';
import 'package:flutter_helmet_app/models/helmet.dart';
import 'package:flutter_helmet_app/screens/detail/widget/add_cart.dart';
import 'package:flutter_helmet_app/screens/detail/widget/detail_app_bar.dart';
import 'package:flutter_helmet_app/screens/detail/widget/helmet_info.dart';
import 'package:flutter_helmet_app/screens/detail/widget/size_list.dart';

class DetailPage extends StatelessWidget {
  final Helmet helmet;
  DetailPage(this.helmet);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DetailAppBar(helmet),
            HelmetInfo(helmet),
            SizeList(),
            AddCart(helmet),
          ],
        ),
      ),
    );
  }
}
