import 'package:flutter/material.dart';
import 'package:flutter_helmet_app/models/helmet.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_helmet_app/screens/detail/widget/color_picker.dart';

class DetailAppBar extends StatefulWidget {
  final Helmet helmet;
  DetailAppBar(this.helmet);

  @override
  State<DetailAppBar> createState() => _DetailAppBarState();
}

class _DetailAppBarState extends State<DetailAppBar> {
  final List<Color> colors = [
    Color(0xFFDBDD00),
    Color(0xFF36E227),
    Color(0xFFFFFFFF),
    Color(0xFF2C2C29),
  ];

  final CarouselController _controller = CarouselController();
  int _currentPage = 0;
  int _currentColor = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            child: CarouselSlider(
              carouselController: _controller,
              options: CarouselOptions(
                height: 500,
                viewportFraction: 1,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentPage = index;
                  });
                },
              ),
              items: widget.helmet.detailUrl
                  .map((e) => Builder(
                        builder: (context) => Container(
                          margin: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('$e'), fit: BoxFit.fitHeight),
                            borderRadius: BorderRadius.circular(25),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).size.width * 0.08,
            left: MediaQuery.of(context).size.width * 0.45,
            child: Row(
              children: widget.helmet.detailUrl
                  .asMap()
                  .entries
                  .map(
                    (entry) => GestureDetector(
                      onTap: () => _controller.animateToPage(entry.key),
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        width: 8,
                        height: 8,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white.withOpacity(
                              _currentPage == entry.key ? 0.9 : 0.4),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          Positioned(
            bottom: 30,
            right: 30,
            child: Container(
              height: 140,
              width: 40,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.3),
                borderRadius: BorderRadius.circular(30),
              ),
              child: ListView.separated(
                  padding: EdgeInsets.zero,
                  itemBuilder: ((context, index) => GestureDetector(
                      onTap: () {
                        setState(() {
                          _currentColor = index;
                        });
                      },
                      child:
                          ColorPicker(_currentColor == index, colors[index]))),
                  separatorBuilder: (_, index) => SizedBox(height: 3),
                  itemCount: colors.length),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top, left: 25, right: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.9),
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 8),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.grey,
                        size: 20,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.more_horiz,
                    color: Colors.grey,
                    size: 20,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
