import 'package:flutter/material.dart';
import 'package:flutter_helmet_app/models/helmet.dart';

class HelmetInfo extends StatelessWidget {
  final Helmet helmet;
  HelmetInfo(this.helmet);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${helmet.title} ${helmet.subtitle}',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    shape: BoxShape.circle),
                child: Icon(
                  Icons.favorite,
                  color: Colors.red,
                  size: 18,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 5, bottom: 10),
            child: Row(
              children: [
                Icon(
                  Icons.star_border,
                  color: Theme.of(context).primaryColor,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5),
                  child: Text(
                    '4.5 (2.7k)',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text:
                      'Pista GP RR is an exact replica of the AGV helmet used in races by professional world championship riders. It has therefore received FIM homologation, which certifies the highest possible level of protection, even against any dangerous twisting of the head. Every detail is designed to ensure ultimate performance. The Extreme Carbon shell in 100% carbon fiber guarantees extraordinary lightness, which make it effortless to wear... ',
                  style: TextStyle(
                    color: Colors.grey.withOpacity(0.7),
                    height: 1.5,
                    fontSize: 14,
                  ),
                ),
                TextSpan(
                  text: 'Read More',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
