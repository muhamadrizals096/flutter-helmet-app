import 'package:flutter/material.dart';
import 'package:flutter_helmet_app/models/helmet.dart';
import 'package:flutter_helmet_app/screens/detail/detail.dart';

class HelmetItem extends StatelessWidget {
  final Helmet helmet;
  HelmetItem(this.helmet);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => DetailPage(helmet)));
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                children: [
                  Container(
                    margin: EdgeInsets.all(8),
                    height: 170,
                    width: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage(helmet.imageUrl),
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    top: 15,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.9),
                          shape: BoxShape.circle),
                      child: Icon(
                        Icons.favorite,
                        color: Colors.red,
                        size: 15,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                helmet.title,
                style: TextStyle(fontWeight: FontWeight.bold, height: 1.5),
              ),
              Text(
                helmet.subtitle,
                style: TextStyle(height: 1.5),
              ),
              Text(
                helmet.price,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    height: 1.5,
                    color: Theme.of(context).primaryColor),
              )
            ],
          ),
        ),
      ),
    );
  }
}
