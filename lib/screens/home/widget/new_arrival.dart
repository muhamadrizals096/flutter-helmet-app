import 'package:flutter/material.dart';
import 'package:flutter_helmet_app/models/helmet.dart';
import 'package:flutter_helmet_app/screens/home/widget/categories_list.dart';
import 'package:flutter_helmet_app/screens/home/widget/helmet_item.dart';

class NewArrival extends StatelessWidget {
  final helmetList = Helmet.generateHelmet();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CategoriesList('New Arrival'),
          Container(
            height: 280,
            child: ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: 22),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => HelmetItem(helmetList[index]),
                separatorBuilder: (_, index) => SizedBox(
                      width: 10,
                    ),
                itemCount: helmetList.length),
          ),
        ],
      ),
    );
  }
}
