import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_helmet_app/screens/home/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return MaterialApp(
      title: 'Flutter_Helmet',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          backgroundColor: Color(0xFFFFAFAFA),
          primaryColor: Color(0xFFF85426),
          accentColor: Color(0xFFFFCFC2)),
      home: HomePage(),
    );
  }
}
